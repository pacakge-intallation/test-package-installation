<?php return array(
    'root' => array(
        'name' => 'digitcare/test',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '19594b508811d948866b0a5dbea00d7eb6a89809',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'digitcare/test' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '19594b508811d948866b0a5dbea00d7eb6a89809',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
